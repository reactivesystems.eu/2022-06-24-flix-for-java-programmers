# Flix For Java Programmers

Source code examples for the blog post "[Flix For Java Programmers](https://www.reactivesystems.eu/2022/06/24/flix-for-java-programmers.html)"


See the Flix [Get Started Page](https://flix.dev/get-started/) to learn how to setup an environment where you can run them. The Flix version they were created with is 0.28.0.


---

*If you like the blog post and/or the code, please follow me on [Twitter](https://twitter.com/lutzhuehnken) or [Mastodon](https://social.tchncs.de/@lutzhuehnken) to be notified about updates. Or, if you're really enthusiastic about it, you can even become a [supporter on Flattr](https://flattr.com/@lutzh).*


June 24, 2022



